package com.vk.challenge.utils

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.content.res.AssetManager
import timber.log.Timber
import java.io.IOException
import java.io.InputStream


object AssetsUtil {
    fun getFullPath(string: String): String = "file:///android_asset/$string"
}