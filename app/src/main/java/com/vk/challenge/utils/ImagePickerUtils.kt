package com.vk.challenge.utils

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.MediaStore

object ImagePickerUtils {

    fun grantPermissionIntent(context: Context, uri: Uri, intent: Intent): Intent {
        val resInfoList = context.packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
        resInfoList
                .map { it.activityInfo.packageName }
                .forEach { context.grantUriPermission(it, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION) }
        return intent
    }

    fun getRealPathFromURI(context: Context, intent: Intent): String? {
        var absolutePath: String? = null

        intent.data?.let {
            val projection = arrayOf(MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME)
            val cursor = context.contentResolver.query(it, projection, null, null, null)
            if (cursor != null) {
                val columnData = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
                cursor.moveToFirst()
                absolutePath = cursor.getString(columnData)
                cursor.close()
            }
        }

        return absolutePath
    }
}