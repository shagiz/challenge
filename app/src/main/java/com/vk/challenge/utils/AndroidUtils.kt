package com.vk.challenge.utils

import android.content.Context
import android.util.DisplayMetrics
import android.view.View

fun convertDpToPixels(dp: Float, context: Context): Float = dp * context.resources.displayMetrics.density
fun convertPixelsToDp(px: Float, context: Context): Float = px / context.resources.displayMetrics.density

fun isViewOverlapping(v1: View, v2: View): Boolean {
    val centerX = (2 * v2.x + v2.width) / 2
    val centerY = (2 * v2.y + v2.height) / 2
    val deltaX = v2.scaleX * (centerX - v2.x)
    val deltaY = v2.scaleY * (centerY - v2.y)
    val scaledX = centerX - deltaX
    val scaledY = centerY - deltaY
    val xW = centerX + deltaX
    val yH = centerY + deltaY

    val x1W = v1.x + v1.width
    val y1H = v1.y + v1.height

    return v1.x < xW
            && x1W > scaledX
            && v1.y < yH
            && y1H > scaledY
}

fun isViewCenterInsideOtherView(v1: View, v2: View): Boolean {
    val centerX = (2 * v2.x + v2.width) / 2
    val centerY = (2 * v2.y + v2.height) / 2

    val x1W = v1.x + v1.width
    val y1H = v1.y + v1.height

    return v1.x < centerX
            && x1W > centerX
            && v1.y < centerY
            && y1H > centerY
}