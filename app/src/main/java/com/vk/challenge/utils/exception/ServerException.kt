package com.vk.challenge.utils.exception

class ServerException(message: String) : IllegalStateException(message)