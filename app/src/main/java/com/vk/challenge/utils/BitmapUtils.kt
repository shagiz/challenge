package com.vk.challenge.utils

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.support.annotation.ColorInt
import android.support.v7.graphics.Palette
import android.view.View
import com.vk.challenge.presenter.MainPresenter
import timber.log.Timber


object BitmapUtils {
    fun setTextColorAsync(bitmap: Bitmap?, body: (@param:ColorInt Int) -> Unit) {
        if (bitmap != null) {
            val paletteListener = Palette.PaletteAsyncListener { palette ->
                run {
                    palette.dominantSwatch?.let {
                        val red = Color.red(it.rgb)
                        val green = Color.green(it.rgb)
                        val blue = Color.blue(it.rgb)
                        if ((red * 0.299 + green * 0.587 + blue * 0.114) > 186) {
                            body(Color.BLACK)
                        } else {
                            body(Color.WHITE)
                        }
                    }
                }
            }
            Palette.from(bitmap).generate(paletteListener)
        }
    }

    fun getBitmapFromView(view: View): Bitmap {
        val originalWidth = view.width
        val originalHeight = view.height

        Timber.d("TORRTLE : [$originalWidth, $originalHeight] : [${view.width}, ${view.height}]")
        val scaleRatio = MainPresenter.SCALED_MIN_SIZE.toFloat() / Math.min(originalWidth, originalHeight)
        val scaledWidth = originalWidth * scaleRatio
        val scaledHeight = originalHeight * scaleRatio

        val bitmap = Bitmap.createBitmap(scaledWidth.toInt(), scaledHeight.toInt(), Bitmap.Config.RGB_565)
        val c = Canvas(bitmap)
        c.scale(scaleRatio, scaleRatio)
        view.draw(c)
        return bitmap
    }
}