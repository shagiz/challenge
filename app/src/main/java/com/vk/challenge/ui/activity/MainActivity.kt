package com.vk.challenge.ui.activity

import android.Manifest
import android.animation.LayoutTransition
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.annotation.ColorInt
import android.support.annotation.DrawableRes
import android.support.design.widget.TabLayout
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v4.graphics.ColorUtils
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import com.tbruyelle.rxpermissions2.RxPermissions
import com.vk.challenge.App
import com.vk.challenge.BuildConfig
import com.vk.challenge.R
import com.vk.challenge.di.DaggerMainComponent
import com.vk.challenge.model.data.Background
import com.vk.challenge.model.data.Sticker
import com.vk.challenge.model.data.Thumbnail
import com.vk.challenge.presenter.MainPresenter
import com.vk.challenge.presenter.view.MainView
import com.vk.challenge.ui.adapter.BackgroundAdapter
import com.vk.challenge.ui.adapter.StickersAdapter
import com.vk.challenge.ui.adapter.ThumbnailsAdapter
import com.vk.challenge.ui.custom.StickersBottomDialog
import com.vk.challenge.utils.BitmapUtils
import com.vk.challenge.utils.ImagePickerUtils
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_bottom_bar.*
import kotlinx.android.synthetic.main.layout_popup.view.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class MainActivity : BaseActivity<MainView, MainPresenter>(), MainView {
    override fun getLayout(): Int = R.layout.activity_main

    @Inject override lateinit var presenter: MainPresenter

    private lateinit var rxPermissions: RxPermissions
    private lateinit var popupWindow: PopupWindow
    private var previousHeightDifference = 0
    private var keyboardHeight = 0
    private var isKeyBoardVisible = false
    private lateinit var popUpView: View
    private var currentAlpha = 0x00

    private var state = WALL_POST
    private var showPopup = false
    private var photoFilePath: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerMainComponent.builder()
                .appComponent(App.appComponent)
                .build()
                .inject(this)

        rxPermissions = RxPermissions(this)

        text_input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence, p1: Int, p2: Int, p3: Int) {
                if (s.isNotEmpty()) {
                    val lp = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
                    lp.addRule(RelativeLayout.CENTER_IN_PARENT)
                    text_input.layoutParams = lp
                    text_input.gravity = Gravity.CENTER
                    send_btn.isEnabled = true
                } else {
                    val lp = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
                    lp.addRule(RelativeLayout.CENTER_IN_PARENT)
                    text_input.layoutParams = lp
                    text_input.gravity = Gravity.START
                    send_btn.isEnabled = false
                }
            }
        })

        send_btn.setOnClickListener {
            toggleKeyboard(false)
            basket.visibility = View.GONE
            text_input.setText(text_input.text.trim())
            text_input.isCursorVisible = false
            text_input.clearComposingText()
            if (state == WALL_POST) {
                presenter.sendWallPost(BitmapUtils.getBitmapFromView(frame))
            } else {
                presenter.sendStoryPost(BitmapUtils.getBitmapFromView(frame))
            }
        }

        for (i in 0 until tab_layout.tabCount) {
            ((tab_layout.getChildAt(0) as LinearLayout)
                    .getChildAt(i) as LinearLayout)
                    .getChildAt(1).scaleY = -1f
        }

        main_view.layoutTransition.enableTransitionType(LayoutTransition.CHANGING)

        tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab) {
                if (tab.position == 0) {
                    state = WALL_POST
                    //resize layout square
                    frame.resizeSquare(true)
                } else {
                    state = STORY_POST
                    //resize layout full
                    frame.resizeSquare(false, layout_bottom_bar.height)
                }
            }
        })

        text_input.setOnClickListener {
            if (popupWindow.isShowing) {
                showPopup = false
            }
        }

        font_changer.setOnClickListener { changeFontBackground(true) }
        stickers_btn.setOnClickListener { presenter.onShowStickersClick() }
        initPopup()

        checkKeyboardHeight(main_view)
        presenter.takeView(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CAMERA_REQUEST_CODE) {
            handleCameraPhoto()
        } else if (requestCode == GALLERY_REQUEST_CODE && data != null) {
            ImagePickerUtils.getRealPathFromURI(this, data)?.let {
                Timber.d("width: ${frame.width} height:${frame.height}")
                presenter.onBackgroundSelected(Background(it, false), 0)
            }
        }
    }

    private fun handleCameraPhoto() {
        photoFilePath?.let {
            presenter.onBackgroundSelected(Background(it, false), 0)
        }
    }

    override fun onBackPressed() {
        if (popupWindow.isShowing) {
            if (isKeyBoardVisible) {
                showPopup = false
            } else {
                popupWindow.dismiss()
            }
        } else {
            super.onBackPressed()
        }
    }

    private fun toggleKeyboard(b: Boolean) {
        if (isKeyBoardVisible || (!isKeyBoardVisible && b)) {
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.toggleSoftInput(0, 0)
        }
    }

    override fun showLoadFinished() {
        loading_progress_text.text = getString(R.string.published)
        progress_bar.visibility = View.INVISIBLE
        img_done.visibility = View.VISIBLE
        new_post.text = getString(R.string.create_new)
        new_post.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        new_post.setTextColor(ContextCompat.getColor(this, R.color.white))
        new_post.setOnClickListener {
            presenter.createNewPost()
        }
    }

    override fun showError(message: String?) {
        loading_progress_text.text = message
        progress_bar.visibility = View.INVISIBLE
        img_done.visibility = View.VISIBLE
        new_post.text = getString(R.string.create_new)
        new_post.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        new_post.setTextColor(ContextCompat.getColor(this, R.color.white))
        new_post.setOnClickListener {
            cancelUpload()
        }
    }

    override fun showLoading() {
        loading_view.visibility = View.VISIBLE
        progress_bar.visibility = View.VISIBLE
        img_done.visibility = View.GONE
        loading_progress_text.text = getString(R.string.publication)
        new_post.text = getString(R.string.cancel)
        new_post.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent))
        new_post.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
        new_post.setOnClickListener {
            presenter.cancelUpload()
        }
    }

    override fun showStickers(stickersPath: List<Sticker>) {
        val adapter = StickersAdapter()
        val dialog = StickersBottomDialog(this, adapter)

        adapter.setDelegate(object : StickersAdapter.StickerDelegate {
            override fun onStickerSelected(sticker: Sticker) {
                frame.addStickerView(sticker, text_input)
                dialog.dismiss()
            }
        })
        adapter.setStickers(stickersPath)
        dialog.show()
    }

    private fun changeFontBackground(moveAlpha: Boolean) {
        val color = text_input.currentTextColor
        val red = Color.red(color)
        val green = Color.green(color)
        val blue = Color.blue(color)

        if (moveAlpha) {
            currentAlpha = if (currentAlpha == NO_ALPHA) ALPHA_50 else if (currentAlpha == ALPHA_50) TRANSPARENT else NO_ALPHA
        }
        val colorAlpha = Color.argb(currentAlpha, NO_ALPHA - red, NO_ALPHA - green, NO_ALPHA - blue)

        if (currentAlpha != TRANSPARENT) {
            text_input.setTextBackgroundColor(colorAlpha)
        } else {
            text_input.setTextBackgroundColor(null)
        }
    }

    private fun initPopup() {
        popUpView = View.inflate(this, R.layout.layout_popup, null)

        val popUpHeight = resources.getDimension(R.dimen.keyboard_height)
        changeKeyboardHeight(popUpHeight.toInt())

        popupWindow = PopupWindow(popUpView,
                ViewGroup.LayoutParams.MATCH_PARENT,
                keyboardHeight,
                false)

        popupWindow.animationStyle = R.style.PopupAnimation

        popupWindow.setOnDismissListener {
            animateResizeMain(true)
            background_footer.visibility = View.GONE
        }
    }

    override fun initPicker(data: List<Background>, currentSelected: Int) {
        val delegate = object : BackgroundAdapter.BackgroundDelegate {
            override fun onTakeFromCameraClick() {
                rxPermissions
                        .request( Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .filter({ it })
                        .subscribe({
                            val file = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                            val photo = File.createTempFile("temp_photo_", "_img.jpg", file)

                            photoFilePath = photo.absolutePath

                            photo?.let {
                                val photoUri = FileProvider.getUriForFile(this@MainActivity, AUTHORITIES, it)

                                val intentCam = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                                intentCam.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)

                                startActivityForResult(ImagePickerUtils.grantPermissionIntent(this@MainActivity, photoUri, intentCam), CAMERA_REQUEST_CODE)
                            }
                        }, { Timber.e(it) })
            }

            override fun onTakeFromFilesClick() {
                rxPermissions
                        .request(Manifest.permission.READ_EXTERNAL_STORAGE)
                        .filter({ it })
                        .subscribe({
                            val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                            startActivityForResult(galleryIntent, GALLERY_REQUEST_CODE)
                        })
            }

            override fun onBackgroundSelected(relativePath: Background, position: Int) {
                presenter.onBackgroundSelected(relativePath, position)
            }
        }
        val adapter = BackgroundAdapter(delegate)
        popUpView.background_recycler.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.HORIZONTAL, false)
        adapter.setData(data, currentSelected)
        popUpView.background_recycler.adapter = adapter
    }

    override fun showBitmapBackground(background: Background) {
        Glide.with(this)
                .load(background.getFullPath())
                .apply(RequestOptions.centerCropTransform())
                .listener(object : RequestListener<Drawable> {
                    override fun onResourceReady(resource: Drawable, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        val bitmap = (resource as BitmapDrawable).bitmap

                        BitmapUtils.setTextColorAsync(bitmap, {
                            text_input.setTextColor(it)
                            text_input.setHintTextColor(ColorUtils.setAlphaComponent(it, 0xC1))
                            changeFontBackground(false)
                        })
                        return false
                    }

                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean = false

                })
                .into(object : SimpleTarget<Drawable>(frame.width, frame.height) {
                    override fun onResourceReady(resource: Drawable?, transition: Transition<in Drawable>?) {
                        frame.background = resource
                    }
                })
    }

    override fun initThumbRecycler(data: List<Thumbnail>) {

        val delegate = object : ThumbnailsAdapter.ThumbDelegate {
            override fun onClearBackgroundClick() {
                frame.setBackgroundColor(Color.WHITE)
                text_input.setTextColor(Color.BLACK)
                text_input.setHintTextColor(ColorUtils.setAlphaComponent(Color.BLACK, 0xC1))
                popupWindow.dismiss()
            }

            override fun onThumbBackgroundClick(thumbnail: Thumbnail) {
                presenter.onThumbBackgroundClick(thumbnail)
                popupWindow.dismiss()
            }

            override fun onMoreBackgroundClick() {
                rxPermissions
                        .request(Manifest.permission.READ_EXTERNAL_STORAGE)
                        .subscribe({ granted ->
                            presenter.onMoreBackgroundClick(granted)
                        })
            }
        }

        recycler_thumbnails.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        val adapter = ThumbnailsAdapter(delegate)
        adapter.setData(data)
        recycler_thumbnails.adapter = adapter
        recycler_thumbnails.setHasFixedSize(true)
    }

    override fun showBackgroundsPicker(background: Background?) {
        if (!popupWindow.isShowing) {
            showPopup = true
            animateResizeMain(false)

            popupWindow.height = keyboardHeight

            popupWindow.showAtLocation(main_view, Gravity.BOTTOM, 0, 0)
        } else {
            showPopup = false
        }

        background?.let {
            showBitmapBackground(it)
        }
    }

    override fun showDrawableTextDepends(@DrawableRes drawableRes: Int, @ColorInt textColor: Int) {
        text_input.setTextColor(textColor)
        text_input.setHintTextColor(ColorUtils.setAlphaComponent(textColor, 0xC1))
        frame.setBackgroundResource(drawableRes)
    }

    override fun clear() {
        frame.clearStickers()
        frame.setBackgroundColor(Color.WHITE)
        text_input.setTextColor(Color.BLACK)
        text_input.setHintTextColor(ColorUtils.setAlphaComponent(Color.BLACK, 0xC1))
        text_input.text.clear()
        text_input.requestFocus()
        text_input.isCursorVisible = true
        loading_view.visibility = View.GONE
        toggleKeyboard(true)
    }

    override fun cancelUpload() {
        loading_view.visibility = View.GONE
    }

    private fun checkKeyboardHeight(parentLayout: View) {

        parentLayout.viewTreeObserver.addOnGlobalLayoutListener {
            val r = Rect()
            parentLayout.getWindowVisibleDisplayFrame(r)

            val screenHeight = getUsableScreenHeight(parentLayout)
            val heightDifference = screenHeight - (r.bottom)

            if (previousHeightDifference - heightDifference > 50) {
                popupWindow.dismiss()
            }

            previousHeightDifference = heightDifference
            if (heightDifference > 100) {
                isKeyBoardVisible = true
                changeKeyboardHeight(heightDifference)
                if (popupWindow.isShowing && !showPopup) {
                    popupWindow.dismiss()
                }
            } else {
                isKeyBoardVisible = false
            }
        }
    }

    private fun getUsableScreenHeight(view: View): Int {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            val metrics = DisplayMetrics()

            val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowManager.defaultDisplay.getMetrics(metrics)

            metrics.heightPixels
        } else {
            view.rootView.height
        }
    }

    private fun getStatusBarHeight(): Int {
        var result = 0
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    private fun changeKeyboardHeight(height: Int) {
        if (height > 100) {
            keyboardHeight = height
            val params = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, keyboardHeight)
            background_footer.layoutParams = params
        }
    }

    private fun animateResizeMain(toFullSize: Boolean) {
        val params: FrameLayout.LayoutParams = if (toFullSize) {
            FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        } else {
            FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, getUsableScreenHeight(main_view.rootView) - getStatusBarHeight() - keyboardHeight)
        }

        main_view.layoutParams = params
    }

    companion object {
        const val WALL_POST = 0
        const val STORY_POST = 1
        const val CAMERA_REQUEST_CODE = 42
        const val GALLERY_REQUEST_CODE = 33
        const val AUTHORITIES = BuildConfig.APPLICATION_ID + ".fileprovider"
        const val NO_ALPHA = 0xFF
        const val ALPHA_50 = 0x80
        const val TRANSPARENT = 0x00

    }
}
