package com.vk.challenge.ui.holder

import android.support.v7.widget.RecyclerView
import android.view.View
import com.vk.challenge.ui.adapter.ThumbnailsAdapter

class SimpleThumbHolder(itemView: View) : RecyclerView.ViewHolder(itemView)