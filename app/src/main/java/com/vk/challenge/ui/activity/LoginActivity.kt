package com.vk.challenge.ui.activity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import com.vk.challenge.App
import com.vk.challenge.R
import com.vk.challenge.di.DaggerLoginComponent
import com.vk.challenge.presenter.LoginPresenter
import com.vk.challenge.presenter.view.LoginView
import com.vk.sdk.VKScope
import com.vk.sdk.VKSdk
import kotlinx.android.synthetic.main.activity_login.*
import timber.log.Timber
import javax.inject.Inject
import com.vk.sdk.api.VKError
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKCallback


class LoginActivity : BaseActivity<LoginView, LoginPresenter>(), LoginView {

    @Inject override lateinit var presenter: LoginPresenter

    override fun getLayout(): Int = R.layout.activity_login

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerLoginComponent.builder()
                .appComponent(App.appComponent)
                .build()
                .inject(this)

        login_btn.setOnClickListener {
            presenter.onLoginClick()
        }

        presenter.takeView(this)
    }

    override fun authVK() {
        VKSdk.login(this, VKScope.PHOTOS, VKScope.WALL)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, object : VKCallback<VKAccessToken> {
            override fun onResult(res: VKAccessToken) {
                if (!TextUtils.isEmpty(res.userId) && !TextUtils.isEmpty(res.accessToken)) {
                    presenter.onLoginSuccess(res.userId, res.accessToken)
                }
            }

            override fun onError(error: VKError) {
                // Произошла ошибка авторизации (например, пользователь запретил авторизацию)
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun startApplication() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}
