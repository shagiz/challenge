package com.vk.challenge.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.vk.challenge.R
import com.vk.challenge.model.data.Sticker
import com.vk.challenge.ui.activity.MainActivity
import com.vk.challenge.ui.holder.StickerHolder
import com.vk.challenge.utils.AssetsUtil

class StickersAdapter : RecyclerView.Adapter<StickerHolder>() {

    private lateinit var mStickers: List<Sticker>
    private lateinit var mDelegate: StickerDelegate
    fun setStickers(stickers: List<Sticker>) {
        mStickers = stickers
        notifyDataSetChanged()
    }

    fun setDelegate(delegate: StickerDelegate) {
        mDelegate = delegate
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StickerHolder {
        val holder = StickerHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_sticker, parent, false))

        holder.itemView.setOnClickListener { mDelegate.onStickerSelected(mStickers[holder.adapterPosition]) }
        return holder
    }

    override fun onBindViewHolder(holder: StickerHolder, position: Int) {
        Glide.with(holder.itemView.context)
                .load(mStickers[position].getFullPath())
                .into(holder.itemView as ImageView)
    }

    override fun getItemCount(): Int = mStickers.size

    interface StickerDelegate {
        fun onStickerSelected(sticker: Sticker)
    }
}