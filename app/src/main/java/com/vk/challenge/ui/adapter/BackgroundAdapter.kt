package com.vk.challenge.ui.adapter

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.vk.challenge.R
import com.vk.challenge.model.data.Background
import com.vk.challenge.ui.holder.BackgroundHolder
import kotlinx.android.synthetic.main.item_backgound.view.*

class BackgroundAdapter(private val delegate: BackgroundDelegate) : RecyclerView.Adapter<BackgroundHolder>() {

    private lateinit var mData: List<Background>
    private var selectedItemPosition: Int = 0

    fun setData(data: List<Background>, currentSelected: Int) {
        mData = data
        if (currentSelected != 0) {
            selectedItemPosition = currentSelected
        } else if (data.size >= 2 && selectedItemPosition == 0) {
            selectedItemPosition = 2
        }
        notifyDataSetChanged()
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView?) {
        recyclerView?.scrollToPosition(selectedItemPosition - 2)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BackgroundHolder {
        val holder = BackgroundHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_backgound, parent, false))
        when (viewType) {
            TAKE_FROM_CAMERA -> {
                holder.itemView.setOnClickListener {
                    delegate.onTakeFromCameraClick()
                    swapSelected(holder.adapterPosition)
                }
            }
            TAKE_FROM_FILES -> {
                holder.itemView.setOnClickListener {
                    delegate.onTakeFromFilesClick()
                    swapSelected(holder.adapterPosition)
                }
            }
            else -> {
                holder.itemView.setOnClickListener {
                    delegate.onBackgroundSelected(mData[holder.adapterPosition - 2], holder.adapterPosition)
                    swapSelected(holder.adapterPosition)
                }
            }
        }
        return holder
    }

    private fun swapSelected(adapterPosition: Int) {
        notifyItemChanged(selectedItemPosition)
        selectedItemPosition = adapterPosition
        notifyItemChanged(selectedItemPosition)
    }

    override fun onBindViewHolder(holder: BackgroundHolder, position: Int) {
        with(holder) {
            if (position == selectedItemPosition) {
                itemView.image.borderColor = ContextCompat.getColor(itemView.context, R.color.colorPrimary)
            } else {
                itemView.image.borderColor = ContextCompat.getColor(itemView.context, R.color.transparent)
            }

            when (getItemViewType(position)) {
                TAKE_FROM_CAMERA -> {
                    itemView.image.scaleType = ImageView.ScaleType.CENTER
                    itemView.image.setImageResource(R.drawable.ic_photopicker_camera)
                }
                TAKE_FROM_FILES -> {
                    itemView.image.scaleType = ImageView.ScaleType.CENTER
                    itemView.image.setImageResource(R.drawable.ic_photopicker_albums)
                }
                else -> {
                    val pas = mData[position - 2].getFullPath()
                    Glide.with(itemView.context)
                            .load(pas)
                            .apply(RequestOptions.centerCropTransform())
                            .into(itemView.image)
                }
            }
        }
    }

    override fun getItemCount(): Int = mData.size + 2

    override fun getItemViewType(position: Int): Int = when (position) {
        0 -> TAKE_FROM_CAMERA
        1 -> TAKE_FROM_FILES
        else -> IMAGE_CONTAINER
    }

    interface BackgroundDelegate {
        fun onBackgroundSelected(relativePath: Background, position: Int)
        fun onTakeFromCameraClick()
        fun onTakeFromFilesClick()
    }

    companion object {
        const val TAKE_FROM_CAMERA = 0
        const val TAKE_FROM_FILES = 1
        const val IMAGE_CONTAINER = 3
    }
}