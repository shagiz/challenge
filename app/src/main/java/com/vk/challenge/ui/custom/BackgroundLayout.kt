package com.vk.challenge.ui.custom

import android.animation.Animator
import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.vk.challenge.R
import com.vk.challenge.model.data.Sticker
import com.vk.challenge.utils.convertDpToPixels
import com.vk.challenge.utils.isViewCenterInsideOtherView
import com.vk.challenge.utils.isViewOverlapping
import kotlinx.android.synthetic.main.activity_main.view.*


class BackgroundLayout : RelativeLayout {

    private var isBasketIntercepted = false
    private var basketIntercepts = 0

    private val onStickerMoveListener: OnStickerMoveListener = object : BackgroundLayout.OnStickerMoveListener {
        var count = 0

        override fun onStickerMoveStart() {
            count++
            if (count == 1) {
                Handler().postDelayed({
                    if (count > 0) {
                        basket.visibility = View.VISIBLE
                        basket.translationY = 0f
                        basket.alpha = 0.4f
                        basket.animate()
                                .translationY(-25f)
                                .alpha(1.0f)
                                .setDuration(50)
                                .start()
                    }
                }, 500)
            }
        }

        override fun onStickerMoveStop() {
            count--
            if (count == 0) {
                basket.visibility = View.GONE
                basket.setImageResource(R.drawable.ic_fab_trash)
                basket.scaleX = 1f
                basket.scaleY = 1f
            }
        }
    }

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)

        val isBasketVisible = basket.visibility == View.VISIBLE
        (0 until childCount)
                .asSequence()
                .map { getChildAt(it) }
                .filterIsInstance<GestureImageView>()
                .filter { isBasketVisible && it.isEnabled }
                .forEach {
                    if (isViewOverlapping(basket, it)) {
                        if (!it.isInBasket) {
                            it.isInBasket = true
                            basketIntercepts++
                        }
                        if (!isBasketIntercepted && basketIntercepts == 1) {
                            isBasketIntercepted = true
                            basket.setImageResource(R.drawable.ic_fab_trash_released)
                            basket.scaleX = 1.0f
                            basket.scaleY = 1.0f
                            basket.animate()
                                    .scaleX(1.2f)
                                    .scaleY(1.2f)
                                    .setDuration(100)
                                    .start()
                        }
                        //true overlap and remove
                        if (isViewCenterInsideOtherView(basket, it)) {
                            it.isEnabled = false
                            it.animate()
                                    .scaleX(0.3f)
                                    .scaleY(0.3f)
                                    .alpha(0.3f)
                                    .setDuration(300)
                                    .setListener(object : Animator.AnimatorListener {
                                        override fun onAnimationRepeat(animation: Animator?) {}
                                        override fun onAnimationEnd(animation: Animator?) {
                                            removeView(it)
                                            resetBasket()
                                        }

                                        override fun onAnimationCancel(animation: Animator?) {}
                                        override fun onAnimationStart(animation: Animator?) {}
                                    })
                                    .start()
                        }

                    } else {
                        if (it.isInBasket) {
                            it.isInBasket = false
                            resetBasket()
                        }
                    }
                }
    }

    private fun resetBasket() {
        basketIntercepts--
        if (basketIntercepts == 0) {
            isBasketIntercepted = false
            basket.setImageResource(R.drawable.ic_fab_trash)
            basket.animate()
                    .scaleX(1.0f)
                    .scaleY(1.0f)
                    .setDuration(100)
                    .start()
        }
    }

    fun addStickerView(sticker: Sticker, frontView: View?) {
        val stickerView = GestureImageView(context)
        stickerView.setOnStickerMoveListener(onStickerMoveListener)
        val params = RelativeLayout.LayoutParams(convertDpToPixels(STICKER_SIZE, context).toInt(), convertDpToPixels(STICKER_SIZE, context).toInt());
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);

        params.leftMargin = 107
        addView(stickerView, params)

        frontView?.let {
            removeView(frontView)
            addView(frontView)
        }

        Glide.with(context)
                .load(sticker.getFullPath())
                .into(stickerView)
    }

    fun resizeSquare(square: Boolean, moveBasketHeight: Int = 0) {
//        val params = if (square) {
//            val h = convertDpToPixels(LAYOUT_SIZE, context).toInt()
//            RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, h)
//        } else {
//            RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
//        }
//        params.addRule(RelativeLayout.CENTER_IN_PARENT)
//        layoutParams = params

        if (square){
            layoutParams.height = convertDpToPixels(LAYOUT_SIZE, context).toInt()
        }else{
            layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
        }

        (basket.layoutParams as RelativeLayout.LayoutParams).bottomMargin = convertDpToPixels(16f, context).toInt() + moveBasketHeight

        requestLayout()
    }

    fun clearStickers() {
        (0 until childCount)
                .map { getChildAt(it) }
                .forEach {
                    if (it is GestureImageView) {
                        removeView(it)
                    }
                }
    }

    interface OnStickerMoveListener {
        fun onStickerMoveStart()
        fun onStickerMoveStop()
    }

    companion object {
        const val STICKER_SIZE = 120f
        const val LAYOUT_SIZE = 328f
    }
}
