package com.vk.challenge.ui.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.shagi.detective.ui.LayoutOwner
import com.vk.challenge.presenter.base.BasePresenter
import com.vk.challenge.presenter.view.BaseView

abstract class BaseActivity<V : BaseView, out T : BasePresenter<V>> : AppCompatActivity(), LayoutOwner {
    abstract val presenter: T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dropView()
    }
}