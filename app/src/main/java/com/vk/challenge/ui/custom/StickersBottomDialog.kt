package com.vk.challenge.ui.custom

import android.content.Context
import android.support.design.widget.BottomSheetDialog
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.view.WindowManager
import com.vk.challenge.R
import com.vk.challenge.ui.adapter.StickersAdapter
import kotlinx.android.synthetic.main.layout_stickers.*

class StickersBottomDialog(context: Context, stickersAdapter: StickersAdapter) : BottomSheetDialog(context) {
    init {
        val view = View.inflate(context, R.layout.layout_stickers, null)


        setContentView(view)


        stickers_recycler.layoutManager = GridLayoutManager(context, 4)
        stickers_recycler.setHasFixedSize(true)
        stickers_recycler.adapter = stickersAdapter
    }
}
