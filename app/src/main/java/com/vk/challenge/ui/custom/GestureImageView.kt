package com.vk.challenge.ui.custom

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.WindowManager
import android.widget.RelativeLayout
import com.vk.challenge.utils.RotationDetector


class GestureImageView : AppCompatImageView {
    var isInBasket = false

    private var xDelta: Int = 0
    private var yDelta: Int = 0
    private val rotationDetector: RotationDetector = RotationDetector(object : RotationDetector.OnRotationGestureListener {
        override fun onRotation(rotationDetector: RotationDetector) {
            rotation = rotationDetector.angle
        }
    }, this)
    private var mScaleFactor = 1f

    private val scaleDetector: ScaleGestureDetector = ScaleGestureDetector(this.context, object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
        override fun onScale(detector: ScaleGestureDetector): Boolean {
            mScaleFactor *= detector.scaleFactor;

            mScaleFactor = Math.max(0.3f, Math.min(mScaleFactor, 3.0f));

            scaleX = mScaleFactor
            scaleY = mScaleFactor
            return true;
        }
    })

    private var onStickerMoveListener: BackgroundLayout.OnStickerMoveListener? = null
    private var isMoving = false

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        return rotationDetector.onTouchEvent(event) && scaleDetector.onTouchEvent(event) && moveEvent(event)
    }

    private fun moveEvent(event: MotionEvent): Boolean {
        val X = event.rawX.toInt()
        val Y = event.rawY.toInt()
        when (event.action and MotionEvent.ACTION_MASK) {
            MotionEvent.ACTION_DOWN -> {
                val lParams = layoutParams as RelativeLayout.LayoutParams
                xDelta = X - lParams.leftMargin
                yDelta = Y - lParams.topMargin
                performClick()
            }
            MotionEvent.ACTION_UP -> {
                if (isMoving) {
                    isMoving = false
                    onStickerMoveListener?.onStickerMoveStop()
                }
            }
            MotionEvent.ACTION_POINTER_DOWN -> {
            }
            MotionEvent.ACTION_POINTER_UP -> {
            }
            MotionEvent.ACTION_MOVE -> {
                if (!isMoving) {
                    onStickerMoveListener?.onStickerMoveStart()
                    isMoving = true
                }
                val layoutParams = layoutParams as RelativeLayout.LayoutParams
                layoutParams.leftMargin = X - xDelta
                layoutParams.topMargin = Y - yDelta
                layoutParams.rightMargin = -250
                layoutParams.bottomMargin = -250
                setLayoutParams(layoutParams)
            }
            MotionEvent.ACTION_CANCEL -> {
                if (isMoving) {
                    isMoving = false
                    onStickerMoveListener?.onStickerMoveStop()
                }
            }
        }
        (parent as RelativeLayout).invalidate()
        return true
    }

    fun setOnStickerMoveListener(onStickerMoveListener: BackgroundLayout.OnStickerMoveListener) {
        this.onStickerMoveListener = onStickerMoveListener
    }

    override fun performClick(): Boolean {
        super.performClick()
        return true
    }
}