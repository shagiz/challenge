package com.vk.challenge.ui.custom

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.CornerPathEffect
import android.graphics.Paint
import android.graphics.Path
import android.graphics.Rect
import android.graphics.RectF
import android.os.Build
import android.support.annotation.ColorInt
import android.support.v7.widget.AppCompatEditText
import android.util.AttributeSet
import com.vk.challenge.utils.convertDpToPixels

class BackgroundEditText : AppCompatEditText {
    private var r = Rect()
    private var p = Paint(Paint.ANTI_ALIAS_FLAG)
    private var mPath = Path()
    private var mRectF = RectF()
    private val mPadding = convertDpToPixels(6f, context)
    private var mTextBackgroundColor: Int? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    init {
        p.style = Paint.Style.FILL_AND_STROKE
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            p.pathEffect = CornerPathEffect(convertDpToPixels(4f, context))
        }
    }

    fun setTextBackgroundColor(@ColorInt color: Int?) {
        mTextBackgroundColor = color
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        mTextBackgroundColor?.let {
            if (text.isNotEmpty()) {
                mPath.reset()
                p.color = it
                for (i in 0 until lineCount) {
                    layout.getLineBounds(i, r)

                    val textWidth = layout.getLineWidth(i)
                    val centerX = Math.round(x + width / 2)

                    val left = centerX - textWidth / 2
                    val right = centerX + textWidth / 2

                    r.set(left.toInt(), r.top, right.toInt(), r.bottom)

                    mRectF.set(left - mPadding, r.top.toFloat(), right + mPadding, r.bottom.toFloat())

                    mPath.addRect(mRectF, Path.Direction.CCW)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        mPath.op(mPath, Path.Op.UNION)
                    }
                }

                mPath.close()

                canvas.drawPath(mPath, p)
            }
        }
        super.onDraw(canvas)
    }
}
