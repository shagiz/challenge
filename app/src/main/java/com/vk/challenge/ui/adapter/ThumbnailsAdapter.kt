package com.vk.challenge.ui.adapter

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.vk.challenge.R
import com.vk.challenge.model.data.Thumbnail
import com.vk.challenge.ui.holder.SimpleThumbHolder
import kotlinx.android.synthetic.main.item_thumbnails.view.*

class ThumbnailsAdapter(private val delegate: ThumbDelegate) : RecyclerView.Adapter<SimpleThumbHolder>() {

    private lateinit var mData: List<Thumbnail>

    private var selectedItem: Int = 0

    fun setData(data: List<Thumbnail>) {
        mData = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleThumbHolder {
        val holder = SimpleThumbHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_thumbnails, parent, false))
        with(holder) {

            if (viewType == SIMPLE_VIEW_TYPE) {
                itemView.setOnClickListener {
                    if (adapterPosition == RecyclerView.NO_POSITION) return@setOnClickListener;

                    notifyItemChanged(selectedItem)
                    selectedItem = adapterPosition
                    notifyItemChanged(selectedItem)

                    if (adapterPosition == 0) {
                        delegate.onClearBackgroundClick()
                    } else {
                        delegate.onThumbBackgroundClick(mData[adapterPosition - 1])
                    }
                }
            } else {
                itemView.setOnClickListener {
                    delegate.onMoreBackgroundClick()
                    notifyItemChanged(selectedItem)
                    selectedItem = adapterPosition
                    notifyItemChanged(selectedItem)
                }
            }

        }

        return holder
    }

    override fun onBindViewHolder(holder: SimpleThumbHolder, position: Int) {
        with(holder) {

            if (position == selectedItem) {
                itemView.thumb.borderColor = ContextCompat.getColor(itemView.context, R.color.colorPrimary)
            } else {
                itemView.thumb.borderColor = ContextCompat.getColor(itemView.context, R.color.transparent)
            }

            when {
                position == 0 -> itemView.thumb.setImageResource(R.color.grey_light)
                getItemViewType(position) == SIMPLE_VIEW_TYPE -> itemView.thumb.setImageResource(mData[position - 1].res)
                else -> {
                    itemView.thumb.setBackgroundColor(ContextCompat.getColor(itemView.context, R.color.colorAccent))
                    itemView.thumb.setImageResource(R.drawable.ic_toolbar_new)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int
            = if (position == itemCount - 1) PLUS_VIEW_TYPE else SIMPLE_VIEW_TYPE


    override fun getItemCount(): Int = mData.size + 2


    interface ThumbDelegate {
        fun onClearBackgroundClick()
        fun onThumbBackgroundClick(thumbnail: Thumbnail)
        fun onMoreBackgroundClick()
    }

    companion object {
        const val SIMPLE_VIEW_TYPE = 0
        const val PLUS_VIEW_TYPE = 1
    }
}


