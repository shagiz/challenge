package com.vk.challenge.model

import com.vk.challenge.model.data.Sticker
import com.vk.challenge.model.source.StickerSource
import com.vk.challenge.model.source.local.StickerLocalSource
import javax.inject.Inject

class StickerRepository
@Inject constructor(private val localSoSource: StickerLocalSource) : StickerSource {
    override fun loadStickers(): List<Sticker> = localSoSource.loadStickers()
}