package com.vk.challenge.model

import com.vk.challenge.di.scope.MainScope
import com.vk.challenge.model.data.Thumbnail
import com.vk.challenge.model.source.ThumbSource
import com.vk.challenge.model.source.local.ThumbLocalSource
import javax.inject.Inject

@MainScope
class ThumbRepository
@Inject internal constructor(private val localSource: ThumbLocalSource) : ThumbSource {
    override fun getThumbList(): List<Thumbnail> = localSource.getThumbList()
}