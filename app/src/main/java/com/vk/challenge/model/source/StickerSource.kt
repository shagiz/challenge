package com.vk.challenge.model.source

import com.vk.challenge.model.data.Sticker

interface StickerSource {
    fun loadStickers(): List<Sticker>
}