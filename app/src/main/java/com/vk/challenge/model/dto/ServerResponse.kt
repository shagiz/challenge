package com.vk.challenge.model.dto

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive

class ServerResponse {

    var server: String? = null
    var photo: String? = null
    var hash: String? = null

    companion object {
        private const val HASH = "hash"
        private const val SERVER = "server"
        private const val PHOTO = "photo"

        fun toServerResponse(jsonObject: JsonObject): ServerResponse {
            val serverResponse = ServerResponse()
            serverResponse.hash = jsonObject.get(HASH).asString
            serverResponse.server = jsonObject.get(SERVER).asString
            serverResponse.photo = jsonObject.get(PHOTO).asString
            return serverResponse
        }
    }
}