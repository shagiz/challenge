package com.vk.challenge.model

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.vk.challenge.model.dto.ServerResponse
import io.reactivex.Flowable
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*
import java.io.InputStream

interface AppService {

    @POST("photos.getWallUploadServer")
    fun getWallUploadServer(
            @Query("access_token") accessToken: String
    ): Flowable<Response<JsonElement>>

    @POST
    @Multipart
    fun uploadPhoto(@Url url: String, @Part photo: MultipartBody.Part): Flowable<Response<JsonElement>>

    @POST("photos.saveWallPhoto")
    fun saveWallPhoto(@Query("access_token") accessToken: String,
                      @Query("user_id") userId: String,
                      @Query("photo") photo: String?,
                      @Query("server") server: String?,
                      @Query("hash") hash: String?): Flowable<Response<JsonElement>>

    @POST("wall.post")
    fun wallPost(@Query("access_token") accessToken: String,
                 @Query("owner_id") userId: String,
                 @Query("attachments") attachment: String): Flowable<Response<JsonElement>>
}