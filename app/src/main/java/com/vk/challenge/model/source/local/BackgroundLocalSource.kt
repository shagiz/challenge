package com.vk.challenge.model.source.local

import com.vk.challenge.model.data.Background
import com.vk.challenge.model.source.BackgroundSource
import javax.inject.Inject
import android.provider.MediaStore
import android.provider.MediaStore.MediaColumns
import android.app.Activity
import android.content.Context
import android.database.Cursor
import android.net.Uri

class BackgroundLocalSource @Inject constructor() : BackgroundSource {
    @Inject lateinit var context: Context

    override fun getBackgrounds(): List<Background> = getAllShownImagesPath()

    private fun getAllShownImagesPath(): List<Background> {
        val uri: Uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val cursor: Cursor?
        val listOfAllImages = ArrayList<Background>()
        val orderBy = MediaStore.Images.Media.DATE_TAKEN
        val projection = arrayOf(MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME)

        cursor = context.contentResolver.query(uri, projection, null, null, orderBy + " DESC LIMIT $LIMIT")

        cursor?.let {
            var absolutePathOfImage: String? = null
            val column_index_data: Int = cursor.getColumnIndexOrThrow(MediaColumns.DATA)

            while (cursor.moveToNext()) {
                absolutePathOfImage = cursor.getString(column_index_data)

                listOfAllImages.add(Background(absolutePathOfImage, false))
            }

            cursor.close()
        }

        return listOfAllImages
    }

    companion object {
        const val LIMIT = 100
    }
}