package com.vk.challenge.model

import android.content.Context
import android.text.TextUtils
import com.vk.challenge.di.scope.AppScope
import com.vk.challenge.model.data.Account
import com.vk.challenge.model.source.AccountDataSource

@AppScope
class AccountDataManager constructor(private val mContext: Context) : AccountDataSource {


    override fun getAccount(): Account? {
        val userId = mContext.getSharedPreferences(AUTH_PREFERENCES, Context.MODE_PRIVATE).getString(USER_ID, null)
        val accessToken = mContext.getSharedPreferences(AUTH_PREFERENCES, Context.MODE_PRIVATE).getString(ACCESS_TOKEN, null)
        if (!TextUtils.isEmpty(userId) && !TextUtils.isEmpty(accessToken)) {
            return Account(userId, accessToken)
        }
        return null
    }

    override fun setAccount(account: Account) {
        val editor = mContext.getSharedPreferences(AUTH_PREFERENCES, Context.MODE_PRIVATE).edit()

        if (!TextUtils.isEmpty(account.userId) && !TextUtils.isEmpty(account.accessToken)) {
            editor.putString(USER_ID, account.userId)
            editor.putString(ACCESS_TOKEN, account.accessToken)
        }

        editor.apply()
    }

    companion object {
        const val AUTH_PREFERENCES = "auth"
        const val USER_ID = "user_id"
        const val ACCESS_TOKEN = "access_token"
    }
}