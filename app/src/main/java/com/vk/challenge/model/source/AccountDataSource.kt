package com.vk.challenge.model.source

import com.vk.challenge.model.data.Account

interface AccountDataSource {
    fun getAccount(): Account?
    fun setAccount(account: Account)
}
