package com.vk.challenge.model.source

import com.vk.challenge.model.data.Background

interface BackgroundSource {
    fun getBackgrounds(): List<Background>
}