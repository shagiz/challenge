package com.vk.challenge.model.data

import com.vk.challenge.utils.AssetsUtil

class Background(private val path: String, private val isAsset: Boolean = true) {
    fun getFullPath() = if (isAsset) AssetsUtil.getFullPath(path) else "file://" + path
}