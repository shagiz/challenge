package com.vk.challenge.model.source.local

import com.vk.challenge.model.data.Sticker
import com.vk.challenge.model.source.StickerSource
import javax.inject.Inject

class StickerLocalSource @Inject constructor() : StickerSource {
    override fun loadStickers(): List<Sticker> {
        return listOf(
                Sticker("stickers/1.png"),
                Sticker("stickers/2.png"),
                Sticker("stickers/3.png"),
                Sticker("stickers/4.png"),
                Sticker("stickers/5.png"),
                Sticker("stickers/6.png"),
                Sticker("stickers/7.png"),
                Sticker("stickers/8.png"),
                Sticker("stickers/9.png"),
                Sticker("stickers/10.png"),
                Sticker("stickers/11.png"),
                Sticker("stickers/12.png"),
                Sticker("stickers/13.png"),
                Sticker("stickers/14.png"),
                Sticker("stickers/15.png"),
                Sticker("stickers/16.png"),
                Sticker("stickers/17.png"),
                Sticker("stickers/18.png"),
                Sticker("stickers/19.png"),
                Sticker("stickers/20.png"),
                Sticker("stickers/21.png"),
                Sticker("stickers/22.png"),
                Sticker("stickers/23.png"),
                Sticker("stickers/24.png")
        )
    }
}