package com.vk.challenge.model.source

import com.vk.challenge.model.data.Thumbnail

interface ThumbSource {
    fun getThumbList(): List<Thumbnail>
}