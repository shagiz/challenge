package com.vk.challenge.model

import com.vk.challenge.di.scope.MainScope
import com.vk.challenge.model.data.Background
import com.vk.challenge.model.source.BackgroundSource
import com.vk.challenge.model.source.local.BackgroundLocalSource
import javax.inject.Inject

@MainScope
class BackgroundRepository
@Inject constructor(private val localSource: BackgroundLocalSource) : BackgroundSource {
    override fun getBackgrounds(): List<Background> = localSource.getBackgrounds()
}