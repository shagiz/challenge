package com.vk.challenge.model.data

data class Account(val userId: String, val accessToken: String)