package com.vk.challenge.model.data

import com.vk.challenge.utils.AssetsUtil

data class Sticker(private val path: String) {
    fun getFullPath() = AssetsUtil.getFullPath(path)
}