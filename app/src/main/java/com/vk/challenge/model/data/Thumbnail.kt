package com.vk.challenge.model.data

import android.support.annotation.ColorInt
import android.support.annotation.DrawableRes

class Thumbnail(@DrawableRes val res: Int,
                @ColorInt val textColor: Int,
                @DrawableRes val resFull: Int = res,
                val background: Background? = null) {
}