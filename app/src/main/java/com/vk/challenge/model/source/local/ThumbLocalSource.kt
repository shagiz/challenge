package com.vk.challenge.model.source.local

import android.graphics.Color
import com.vk.challenge.R
import com.vk.challenge.model.data.Background
import com.vk.challenge.model.data.Thumbnail
import com.vk.challenge.model.source.ThumbSource
import javax.inject.Inject

class ThumbLocalSource @Inject constructor() : ThumbSource {
    override fun getThumbList(): List<Thumbnail> = listOf(
            Thumbnail(R.drawable.gradient_blue, Color.BLACK, R.drawable.gradient_blue),
            Thumbnail(R.drawable.gradient_green, Color.BLACK, R.drawable.gradient_green),
            Thumbnail(R.drawable.gradient_orange, Color.WHITE, R.drawable.gradient_orange),
            Thumbnail(R.drawable.gradient_red, Color.WHITE, R.drawable.gradient_red),
            Thumbnail(R.drawable.gradient_purple, Color.WHITE, R.drawable.gradient_purple),
            Thumbnail(R.drawable.thumb_beach, Color.WHITE, R.drawable.background_beach_drawable),
            Thumbnail(R.drawable.thumb_stars, Color.WHITE, R.drawable.thumb_stars, Background("img/stars.png", true))
    )
}