package com.vk.challenge.di

import com.vk.challenge.di.module.MainFragmentModule
import com.vk.challenge.di.scope.MainScope
import com.vk.challenge.ui.activity.MainActivity
import dagger.Component

@MainScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(MainFragmentModule::class))
interface MainComponent {
    fun inject(mainActivity: MainActivity)
}