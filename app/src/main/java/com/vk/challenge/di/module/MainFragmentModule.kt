package com.vk.challenge.di.module

import com.vk.challenge.di.scope.MainScope
import com.vk.challenge.presenter.MainPresenter
import com.vk.challenge.presenter.view.MainView
import dagger.Module
import dagger.Provides

@Module
class MainFragmentModule(val view: MainView) {

    @MainScope
    @Provides
    fun provideMainView(): MainView = view
}