package com.vk.challenge.di

import com.vk.challenge.di.scope.LoginScope
import com.vk.challenge.ui.activity.LoginActivity
import dagger.Component

@LoginScope
@Component(dependencies = arrayOf(AppComponent::class))
interface LoginComponent {
    fun inject(loginActivity: LoginActivity)
}