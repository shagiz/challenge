package com.vk.challenge.di.module

import android.content.Context
import com.vk.challenge.di.scope.AppScope
import com.vk.challenge.model.AccountDataManager
import dagger.Module
import dagger.Provides

@Module
class AccountModule {
    @Provides
    @AppScope
    fun provideAccountManager(context: Context) = AccountDataManager(context)
}