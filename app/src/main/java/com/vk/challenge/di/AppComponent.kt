package com.vk.challenge.di

import android.content.Context
import com.vk.challenge.App
import com.vk.challenge.di.module.AccountModule
import com.vk.challenge.di.module.ContextModule
import com.vk.challenge.di.module.NetworkModule
import com.vk.challenge.di.scope.AppScope
import com.vk.challenge.model.AccountDataManager
import com.vk.challenge.model.AppService

import dagger.Component

@AppScope
@Component(modules = arrayOf(ContextModule::class, NetworkModule::class, AccountModule::class))
interface AppComponent {

    fun getContext(): Context

    fun getAppService(): AppService

    fun getAccountManager(): AccountDataManager

    fun inject(app: App)

}
