package com.vk.challenge.di.scope

import javax.inject.Scope

@Scope
@Retention
annotation class MainScope