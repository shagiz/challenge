package com.vk.challenge.presenter.view

interface LoginView : BaseView {
    fun startApplication()
    fun authVK()
}