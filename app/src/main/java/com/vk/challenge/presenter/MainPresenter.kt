package com.vk.challenge.presenter

import android.graphics.Bitmap
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.vk.challenge.di.scope.MainScope
import com.vk.challenge.model.*
import com.vk.challenge.model.data.Background
import com.vk.challenge.model.data.Thumbnail
import com.vk.challenge.model.dto.ServerResponse
import com.vk.challenge.presenter.base.BasePresenter
import com.vk.challenge.presenter.view.MainView
import com.vk.challenge.utils.exception.ServerException
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import java.io.ByteArrayOutputStream
import javax.inject.Inject

@MainScope
class MainPresenter @Inject constructor() : BasePresenter<MainView>() {

    @Inject lateinit var appService: AppService
    @Inject lateinit var thumbRepository: ThumbRepository
    @Inject lateinit var backgroundRepository: BackgroundRepository
    @Inject lateinit var stickerRepository: StickerRepository
    @Inject lateinit var accountManager: AccountDataManager

    private var selectedBackground: Background? = null
    private var selectedBackgroundPosition: Int = 0
    private var firstBackground: Background? = null

    override fun onViewAttached(view: MainView) {
        view.initThumbRecycler(thumbRepository.getThumbList())
    }

    fun onThumbBackgroundClick(thumbnail: Thumbnail) {
        if (thumbnail.background == null) {
            view?.showDrawableTextDepends(thumbnail.resFull, thumbnail.textColor)
        } else {
            view?.showBitmapBackground(thumbnail.background)
        }
    }

    fun onMoreBackgroundClick(isPermissionGranted: Boolean) {
        val backgrounds: List<Background>

        if (isPermissionGranted) {
            backgrounds = backgroundRepository.getBackgrounds()

            if (selectedBackground == null && backgrounds.isNotEmpty()) {
                firstBackground = backgrounds[0]
                selectedBackground = firstBackground
            }
        } else {
            backgrounds = emptyList()
        }
        view?.initPicker(backgrounds, selectedBackgroundPosition)
        view?.showBackgroundsPicker(selectedBackground)
    }

    fun onBackgroundSelected(background: Background, position: Int) {
        selectedBackground = background
        selectedBackgroundPosition = position
        view?.showBitmapBackground(background)
    }

    fun onShowStickersClick() {
        view?.showStickers(stickerRepository.loadStickers())
    }

    fun sendWallPost(bitmapFromView: Bitmap) {
        accountManager.getAccount()?.let {
            val userId = it.userId
            val accessToken = it.accessToken
            view?.showLoading()
            unSubscribeOnDrop(appService.getWallUploadServer(accessToken)
                    .flatMap {
                        if (it.errorBody() == null && it.body() != null) {
                            Flowable.just(getUploadUrl(it))
                        } else {
                            Flowable.error(ServerException("No url from server"))
                        }
                    }
                    .flatMap { uploadFile(it, bitmapFromView) }
                    .flatMap {
                        if (it.errorBody() == null && it.body() != null) {
                            Flowable.just(toServerResponse(it.body()?.asJsonObject))
                        } else {
                            Flowable.error(ServerException("No photo response from server"))
                        }
                    }
                    .flatMap {
                        if (it.hash != null && it.photo != null && it.server != null) {
                            appService.saveWallPhoto(accessToken, userId, it.photo, it.server, it.hash)
                        } else {
                            Flowable.error(ServerException("wrong params"))
                        }
                    }
                    .flatMap {
                        if (it.errorBody() == null && it.body() != null) {
                            val attachment = getPhotoId(it.body())
                            attachment?.let {
                                appService.wallPost(accessToken, userId, it)
                            } ?: Flowable.error(ServerException("No photo id returned from server"))
                        } else {
                            Flowable.error(ServerException("Server error"))
                        }
                    }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        view?.showLoadFinished()
                    }, {
                        view?.showError(it.message)
                    }))

        }
    }

    fun sendStoryPost(bitmapFromView: Bitmap) {
        //save wall as story
        sendWallPost(bitmapFromView)
    }

    private fun toServerResponse(body: JsonObject?): ServerResponse {
        body?.let {
            return ServerResponse.toServerResponse(body)
        }
        return ServerResponse()
    }

    private fun getUploadUrl(it: Response<JsonElement>) =
            (it.body() as JsonObject).get(RESPONSE).asJsonObject.get(UPLOAD_URL).asString

    private fun getPhotoId(jsonElement: JsonElement?): String? {
        return jsonElement?.asJsonObject?.get(RESPONSE)?.asJsonArray?.get(0)?.asJsonObject?.get(ATTACHMENT_ID)?.asString
    }

    private fun scaleBitmap(bitmap: Bitmap): Bitmap {
        val originalWidth = bitmap.width
        val originalHeight = bitmap.height

        val scaleRatio = SCALED_MIN_SIZE.toFloat() / Math.min(originalWidth, originalHeight)
        val scaledWidth = originalWidth * scaleRatio
        val scaledHeight = originalHeight * scaleRatio

        return Bitmap.createScaledBitmap(bitmap, scaledWidth.toInt(), scaledHeight.toInt(), true)
    }

    private fun uploadFile(url: String, bitmap: Bitmap): Flowable<Response<JsonElement>> {
        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes)
        val request = RequestBody.create(MediaType.parse(MEDIA_TYPE_IMAGE), bytes.toByteArray())
        val body = MultipartBody.Part.createFormData("photo", "photo.png", request)
        return appService.uploadPhoto(url, body)
    }

    fun cancelUpload() {
        dispose()
        view?.cancelUpload()
    }

    fun createNewPost() {
        selectedBackground = firstBackground
        view?.initThumbRecycler(thumbRepository.getThumbList())
        view?.initPicker(backgroundRepository.getBackgrounds(), selectedBackgroundPosition)
        view?.clear()
    }

    companion object {
        const val RESPONSE = "response"
        const val UPLOAD_URL = "upload_url"
        const val MEDIA_TYPE_IMAGE = "image/*"
        const val ATTACHMENT_ID = "id"
        const val SCALED_MIN_SIZE = 1080
    }
}