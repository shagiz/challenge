package com.vk.challenge.presenter.base

import android.support.annotation.CallSuper
import com.vk.challenge.presenter.view.BaseView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import timber.log.Timber

abstract class BasePresenter<V : BaseView>() {
    private var composite = CompositeDisposable()

    var view: V? = null

    @CallSuper
    open fun takeView(view: V) {
        this.view = view;
        onViewAttached(view)
    }

    open fun onViewAttached(view: V) {}

    fun unSubscribeOnDrop(disposable: Disposable) {
        composite.add(disposable)
    }

    fun dispose() {
        if (!composite.isDisposed) {
            composite.dispose()
            composite = CompositeDisposable()
        }
    }

    @CallSuper
    open fun dropView() {
        view = null;
        dispose()
    }
}