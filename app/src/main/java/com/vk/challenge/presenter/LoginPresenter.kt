package com.vk.challenge.presenter

import com.vk.challenge.di.scope.LoginScope
import com.vk.challenge.model.AccountDataManager
import com.vk.challenge.model.data.Account
import com.vk.challenge.presenter.base.BasePresenter
import com.vk.challenge.presenter.view.LoginView
import javax.inject.Inject

@LoginScope
class LoginPresenter @Inject constructor() : BasePresenter<LoginView>() {

    @Inject lateinit var mAccountDataManager: AccountDataManager

    override fun onViewAttached(view: LoginView) {
        if (mAccountDataManager.getAccount() != null) {
            view.startApplication()
        }
    }

    fun onLoginClick() {
        view?.authVK()
    }

    fun onLoginSuccess(userId: String, accessToken: String) {
        mAccountDataManager.setAccount(Account(userId, accessToken));
        view?.startApplication()
    }
}