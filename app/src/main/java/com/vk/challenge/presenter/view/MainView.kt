package com.vk.challenge.presenter.view

import android.support.annotation.ColorInt
import android.support.annotation.DrawableRes
import com.vk.challenge.di.scope.MainScope
import com.vk.challenge.model.data.Background
import com.vk.challenge.model.data.Sticker
import com.vk.challenge.model.data.Thumbnail

interface MainView : BaseView {
    fun initThumbRecycler(data: List<Thumbnail>)
    fun clear()
    fun showDrawableTextDepends(@DrawableRes drawableRes: Int, @ColorInt textColor: Int)
    fun showBackgroundsPicker(background: Background?)
    fun initPicker(data: List<Background>, currentSelected: Int)
    fun showBitmapBackground(background: Background)
    fun showStickers(stickersPath: List<Sticker>)
    fun showLoading()
    fun showError(message: String?)
    fun showLoadFinished()
    fun cancelUpload()
}