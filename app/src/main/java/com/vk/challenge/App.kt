package com.vk.challenge

import android.app.Application
import com.facebook.stetho.Stetho
import com.vk.challenge.di.AppComponent
import com.vk.challenge.di.DaggerAppComponent
import com.vk.challenge.di.module.ContextModule
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKAccessTokenTracker
import com.vk.sdk.VKSdk
import timber.log.Timber

class App : Application() {

    private val vkAccessToken = object : VKAccessTokenTracker() {
        override fun onVKAccessTokenChanged(oldToken: VKAccessToken?, newToken: VKAccessToken?) {

        }
    }

    override fun onCreate() {
        super.onCreate()
        Stetho.initialize(Stetho.newInitializerBuilder(this)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                .build());
        Timber.plant(Timber.DebugTree())
        vkAccessToken.startTracking()
        VKSdk.initialize(this)
        buildComponent()
    }

    private fun buildComponent() {
        appComponent = DaggerAppComponent.builder()
                .contextModule(ContextModule(this))
                .build()

        appComponent.inject(this)
    }

    companion object {
        lateinit var appComponent: AppComponent
            private set
    }
}
